
function init() {
    const { isMobile } = helpers;


    // Carousels
    let carousels = [
        isMobile ? {
            selector: '.negative-mt', 
            options: {
                slidesToShow: 1.1,
                draggable: true,
            }
        } : {},
    ]

    // carouselInstancing(carousels)


    confettiInit()    

}

// window load binds 
window.onload = init;

function DOMLoaded() {
    // these are not always necessary but sometimes they fuck with ya
    if (helpers.iOS) {
        document.querySelector('html').classList.add('ios');
    } else if (helpers.IE()) {
        document.querySelector('html').classList.add('ie');
    }
}

// domcontent binds 
document.addEventListener('DOMContentLoaded', DOMLoaded);