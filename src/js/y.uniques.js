// unique scripts go here.

const confettiInit = () => {
    let duration = 999 * 1000;
    let animationEnd = Date.now() + duration;
    let skew = 1;

    const canvas = document.createElement('canvas');
    canvas.width = 1920;
    canvas.height = 680;

    document.getElementById('confettiWrapper').appendChild(canvas);

    (function frame() {
        var timeLeft = animationEnd - Date.now();
        var ticks = Math.max(200, 500 * (timeLeft / duration));
        skew = Math.max(0.8, skew - 0.001);

        let confettiInst = confetti.create(canvas, {
            resize: true,
            useWorker: true,
        });

        confettiInst({
            particleCount: 3,
            startVelocity: .5,
            ticks: ticks,
            origin: {
                x: Math.random(),
                // since particles fall down, skew start toward the top
                y: (Math.random() * skew) - 0.2
            },
            colors: ['#461BFF', '#FFBE00', '#00D896', '#100C5E', '#00ADFF'],
            shapes: ['square'],
            gravity: randomInRange(0.4, 0.6),
            scalar: randomInRange(.6, 1.1),
            drift: randomInRange(.6, .7)
        });

        if (timeLeft > 0) {
            requestAnimationFrame(frame);
        }
    }());
}


/**
 * @method carouselInstancing | Instances carousels based on array of objects
 * @description Instances carousels based on array of objects
 * @param {Array} carouselArray | an array of objects with selectors and options for each carousel instance
*/

const carouselInstancing = (carouselArray) => {
    let gliderInstances = [];

    carouselArray.forEach( (carousel, idx) => {
        if (carousel !== null) {
            document.querySelector(carousel.selector) !== null ?
                gliderInstances[idx] = new Glider(document.querySelector(carousel.selector), carousel.options) :
                false
        }
    });
}
